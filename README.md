WebRTCtalk
===========

WebRTC vieo conferencing for Drupal.

Instructions
------------

Unpack in the *modules* folder (currently in the root of your Drupal 8
installation) and enable in `/admin/modules`.

Then, visit `/admin/config/media/webrtctalk` for configurations

More to come ....

Attention
---------

This module is a work in progress. Check back.
