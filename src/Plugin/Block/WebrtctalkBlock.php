<?php

namespace Drupal\webrtctalk\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the webrtc block holding html for the screens.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "webrtctalk_session",
 *   admin_label = @Translation("Webrtc Session block")
 * )
 */
class WebrtctalkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'webrtctalk_session_block' => $this->t('<div id="webrtctalk-block"><video></video></div>'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['webrtctalk_video_block'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Webrtc Video'),
      '#description' => $this->t('Webrtc video block'),
      '#default_value' => $this->configuration['webrtctalk_session_block'],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['webrtctalk_session_block']
      = $form_state->getValue('webrtctalk_video_block');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->configuration['webrtctalk_session_block'],
    );
  }

}