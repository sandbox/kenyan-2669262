<?php

namespace Drupal\webrtctalk\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'WebrtctalkSession' action.
 *
 * @Action(
 *  id = "webrtctalk_session",
 *  label = @Translation("Webrtctalk session"),
 *  type = "node",
 * )
 */
class WebrtctalkSession extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    // Insert code here.
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = $object->status->access('edit', $account, TRUE)
      ->andIf($object->access('update', $account, TRUE));

    return $return_as_object ? $access : $access->isAllowed();
  }

}
