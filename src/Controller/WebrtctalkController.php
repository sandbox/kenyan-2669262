<?php

namespace Drupal\webrtctalk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class WebrtctalkController extends ControllerBase {

  /**
   * {@inheritdoc}
   
  public function __construct(WebrtctalkSessionService $WebrtctalkSessionService) {
    $this->WebrtctalkSessionService = $WebrtctalkSessionService;            
  }
 
  /**
   * {@inheritdoc}
   
  public static function create(ContainerInterface $container) {
    return new static(
     $container->get('webrtctalk.session_service')
    );
  }    
    /**
     * Constructs a page with descriptive content.
     *
     * Our router maps this method to the path 'examples/page-example'.
     */
    public function webrtctalk($title, $id) {
        $webrtctalk_start_session = Link::createFromRoute($this->t('Start Session'), 'webrtctalk_start_session')->toString();
        $webrtctalk_start_session1 = Link::createFromRoute($this->t('Stop Session'), 'webrtctalk_stop_session')->toString();
        // Now the arguments page.
        $webrtctalk_stop_session = Url::fromRoute('page_example_arguments', array('first' => '23', 'second' => '56'));

        // Assemble the markup.
        $build = [
       //     '#markup' => $this->WebrtctalkSessionService->getWebrtctalkSessionId(),
            '#markup' => $this->t('
<p>Start/Stop sessions below".</p>
<p>@start_session</p>
<p>@stop_session</p>
<p>@stop_session1</p>

',
                [
                    '@start_session' => $webrtctalk_start_session,
                    '@stop_session' => $webrtctalk_start_session1,
                    '@stop_session1' => $webrtctalk_stop_session,
                ]
            ),
        ];

        return $build;
    }

    /**
     * Constructs a simple page.
     *
     * The router _controller callback, maps the path
     * 'examples/page-example/simple' to this method.
     *
     * _controller callbacks return a renderable array for the content area of the
     * page. The theme system will later render and surround the content with the
     * appropriate blocks, navigation, and styling.
     */
    public function start_session() {
    return [
      '#theme' => 'node__webrtctalk',
      '#attached' => [ 
        'library' => [
          'webrtctalk/webrtctalk',
        ]
      ]
    ];
    }

    public function stop_session() {
    return [
      '#theme' => 'node__webrtctalk',
      '#attached' => [ 
        'library' => [
          'webrtctalk/webrtctalk',
        ]
      ]
    ];
    }

    /**
     * A more complex _controller callback that takes arguments.
     *
     * This callback is mapped to the path
     * 'examples/page-example/arguments/{first}/{second}'.
     *
     * The arguments in brackets are passed to this callback from the page URL.
     * The placeholder names "first" and "second" can have any value but should
     * match the callback method variable names; i.e. $first and $second.
     *
     * This function also demonstrates a more complex render array in the returned
     * values. Instead of rendering the HTML with theme('item_list'), content is
     * left un-rendered, and the theme function name is set using #theme. This
     * content will now be rendered as late as possible, giving more parts of the
     * system a chance to change it if necessary.
     *
     * Consult @link http://drupal.org/node/930760 Render Arrays documentation
     * @endlink for details.
     *
     * @param string $first
     *   A string to use, should be a number.
     * @param string $second
     *   Another string to use, should be a number.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     *   If the parameters are invalid.
     */
    public function webrtctalk2($title, $id) {
        // Make sure you don't trust the URL to be safe! Always check for exploits.
        if (!is_numeric($title) || !is_numeric($id)) {
            // We will just show a standard "access denied" page in this case.
            throw new AccessDeniedHttpException();
        }

        $list[] = $this->t("First number was @number.", array('@number' => $title));
        $list[] = $this->t("Second number was @number.", array('@number' => $id));
        $list[] = $this->t('The total was @number.', array('@number' => $title + $id));

        $render_array['page_example_arguments'] = array(
            // The theme function to apply to the #items.
            '#theme' => 'item_list',
            // The list itself.
            '#items' => $list,
            '#title' => $this->t('Argument Information'),
        );
        return $render_array;
    }
}
